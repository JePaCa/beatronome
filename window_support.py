#! /usr/bin/env python
#
# Support module generated by PAGE version 4.9
# In conjunction with Tcl version 8.6
#    Aug 09, 2017 04:28:32 AM
#    Aug 09, 2017 04:42:51 AM
#    Aug 09, 2017 04:56:24 AM
#    Aug 09, 2017 09:34:36 PM


import sys
import keyboard

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = 0
except ImportError:
    import tkinter.ttk as ttk
    py3 = 1

try:
    import tkFileDialog as filedialog
except ImportError:
    import tkinter.filedialog as filedialog

_keys = {}
_log = []
_record = []
_length = 0
_timer = 0
_inputindex = 0
_starttime = None
_recording = False

_INPUTWINDOW = 3/60.0
_TIMEUNIT = 1

def set_Tk_var():
    global recording_progress
    recording_progress = IntVar()
    global recording_status
    recording_status = StringVar()
    global recording_progress_text
    recording_progress_text = StringVar()
    global key_alias
    key_alias = StringVar()
    global record_list
    record_list = StringVar()
    global keys_list
    keys_list = StringVar()
    global record_name
    record_name = StringVar()
    # Input Window variables
    global next_input_list
    next_input_list = StringVar()
    global current_input
    current_input = StringVar()


def new_record():
    #print('window_support.new_record')
    #sys.stdout.flush()
    global _keys, _log, _length, _starttime, _recording
    global key_alias, keys_list, record_list

    _keys = {}
    _log = []
    _record = []
    _length = 0
    _timer = 0
    _inputindex = 0
    _starttime = None
    _recording = False
    key_alias.set("")
    keys_list.set(("",))
    record_list.set(("",))

def add_key():
    #print('window_support.add_key')
    #sys.stdout.flush()
    global _keys
    global key_alias, keys_list
    evt = keyboard.read_key()
    if evt.name not in _keys:
        _keys[evt.name] = [False, key_alias.get(), None]
        key_alias.set("")
        keys_list.set(tuple(i + " - " + j[1] if j[1] else i for i, j in _keys.items()))

def load_record():
    #print('window_support.load_record')
    #sys.stdout.flush()
    name = filedialog.askopenfile(filetypes=[("JSON", ".json")])
    if name is None:
        return
    import json
    file_content = json.loads(name.read())

    global record_name, recording_progress
    global _log, _length, _starttime
    record_name.set(file_content['name'])
    recording_progress.set("Stopped")
    _log = file_content['recording']
    _length = file_content['length']
    _starttime = 0
    record_list.set(tuple(str(i[0]) + " — " + (i[1] if i[1] else i[2]) + " " + i[3] for i in _log))

def remove_key():
    #print('window_support.remove_key')
    #sys.stdout.flush()
    global _keys
    global keys_list
    global w
    key = w.KeyBox.get("active").split(" - ")[0]
    del _keys[key]
    keys_list.set(tuple(i + " - " + j[1] if j[1] else i for i, j in _keys.items()))

def save_record():
    #print('window_support.save_record')
    #sys.stdout.flush()

    name = filedialog.asksaveasfile(filetypes=[("JSON", ".json")])
    if name is None:
        return
    length = _log[-1][0]

    import json
    file_content = json.dumps({'name': record_name.get(), 'length': length, 'recording': _log})
    name.write(file_content)
    name.close()


import inputwind
def display_input():
    global w, root, recording_progress, recording_progress_text
    global _timer, _inputindex
    if not inputwind.w:
        w.input_window = inputwind.create_Input_Window(root)

    if _inputindex >= len(_log):
        _inputindex = len(_log) - 1

    j = _log[_inputindex]
    time = _timer/1000.0
    recording_progress.set(time/_length * 100)
    recording_progress_text.set(str(min(round(time/_length * 100, 0), 100)) + "%")
    _timer += _TIMEUNIT
    if time == j[0]:
        string = (str(j[0]) + " — " + str(j[1] if j[1] else j[2]) + " " + str(j[3]))
        k = 1
        while True:
            if _inputindex + k >= len(_log) or _log[_inputindex + k][0] > time:
                break
            j = _log[_inputindex + k]
            k += 1
            string += ", " + (str(j[0]) + " — " + str(j[1] if j[1] else j[2]) + " " + str(j[3]))
        _inputindex += k
        w.input_window.current_input.set(string)
        w.input_window.next_input_list.set(tuple(str(i[0]) + " — " + str(i[1] if i[1] else i[2]) + " " + str(i[3]) for i in _log[_inputindex:]))
    w.input_window.time_label.set(str(time))
    if time > (_length + 1):
        w.input_window.close()
        _timer = 0
        _inputindex = 0
        inputwind.w = None
        return
    root.after(_TIMEUNIT, display_input)

def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top

    def keydown_record(event):
        global _keys
        if event.name not in _keys: #If it's not a key we're tracking, don't do anything.
            return False, False

        if _keys[event.name][0] != False: #If the key's already down, don't do anything.
            return False, False

        _keys[event.name][0] = event.time
        _keys[event.name][2] = len(_log)
        return True, True

    def keyup_record(event):
        global _keys, _log
        if event.name not in _keys: #If it's not a key we're tracking, don't do anything.
            return False, False

        if not _keys[event.name][0]: #If the key's already up, don't do anything.
            return False, False

        if (event.time - _keys[event.name][0]) < _INPUTWINDOW: #If the time between the down and up-presses is less than <3 Frames>, don't record the key-up.
            _log[_keys[event.name][2]][3] = 'tap'
            _keys[event.name][0] = False
            return False, True
        else:
            _log[_keys[event.name][2]][3] = 'hold'
            _keys[event.name][0] = False
            _keys[event.name][2] = 'release'
            return True, True

    def key_record(event):
        global recording_status
        global _recording, _starttime, _keys
        if event.event_type == keyboard.KEY_UP and event.name == 'esc':
            _recording = not _recording
            recording_status.set("Recording" if _recording else "Stopped")
            if _starttime is None: #Time is relative to the start of the recording sessions.
                _starttime = event.time
            if not _recording and len(_log) > 0: #If recording is over, set the length of the whole record based on the time to the last recorded input.
                length = _log[-1][0]
            return

        if event.event_type == keyboard.KEY_UP and event.name == 'left ctrl' and not _recording and len(_log) > 0:
            display_input()

        status, list_update = False, False
        if event.event_type == keyboard.KEY_DOWN:
            status, list_update = keydown_record(event)
        elif event.event_type == keyboard.KEY_UP:
            status, list_update = keyup_record(event)

        if status:
            _log.append([round(event.time - _starttime, 3), _keys[event.name][1], event.name, _keys[event.name][2]])
        if list_update:
            record_list.set(tuple(str(i[0]) + " — " + str(i[1] if i[1] else i[2]) + " " + str(i[3]) for i in _log))

    keyboard.hook(key_record)

def destroy_window():
    # Function which closes the window.
    global top_level
    top_level.destroy()
    top_level = None

if __name__ == '__main__':
    import window
    window.vp_start_gui()



